package Utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Set;

public class OtherUtils {

    public static void printCookie(Cookie c) {
        System.out.println("Cookie= " + c.getName() +" : " + c.getValue());
        System.out.println(c.getExpiry());
    }

    public static void printCookies(WebDriver driver) {
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("The number of cookies found are: " + cookies.size());
        for (Cookie c : cookies) {
            printCookie(c);
        }
    }

    public static void takeScreenShot(WebDriver driver) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(SeleniumUtils.screenShotPath + "\\screenshot-" + timestamp.getTime() + ".png");
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            System.out.println("File could not be saved");
        }
    }
}
