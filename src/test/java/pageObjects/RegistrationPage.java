package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {

    private WebDriver driver;
    WebDriverWait wait;
    @FindBy(how = How.ID, using = "register-tab")
    WebElement clickRegisterTab;
    @FindBy(how = How.ID, using = "inputFirstName")
    WebElement firstNameInput;
    @FindBy(how = How.ID, using = "inputLastName")
    WebElement lastNameInput;
    @FindBy(how = How.ID, using = "inputEmail")
    WebElement emailInput;
    @FindBy(how = How.ID, using = "inputUsername")
    WebElement userNameInput;
    @FindBy(how = How.ID, using = "inputPassword")
    WebElement userPassword;
    @FindBy(how = How.ID, using = "inputPassword2")
    WebElement confirmPassword;
    @FindBy(how = How.ID, using = "register-submit")
    WebElement submitRegister;


    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }


    public void register(String firstname, String lastname, String email, String username, String password, String confirmpassword) {
        firstNameInput.clear();
        firstNameInput.sendKeys(firstname);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastname);
        emailInput.clear();
        emailInput.sendKeys(email);
        userNameInput.clear();
        userNameInput.sendKeys(username);
        userPassword.clear();
        userPassword.sendKeys(password);
        confirmPassword.clear();
        confirmPassword.sendKeys(confirmpassword);
        submitRegister.submit();
    }


    public void waitForRegisterPage() {
        wait.until(ExpectedConditions.elementToBeClickable(submitRegister));
    }


    public void openRegistrationPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
        clickRegisterTab.click();

    }


}
