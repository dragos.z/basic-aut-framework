package tests;

import Utils.OtherUtils;
import Utils.SeleniumUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class AdvancedSeleniumTests extends BaseTest {

    @Test
    public void browserTest() {
        driver.get("https://google.com");

    }

    @Test
    public void cookieTest(){
        driver.get("https://google.com");
        OtherUtils.printCookies(driver);
        try {
            OtherUtils.printCookie(driver.manage().getCookieNamed("CONSENT1"));
        }
        catch (Exception e) {
            System.out.println("Cookie not found !!!");
        }
        Cookie cookie = new Cookie("myCookie", "cookie123");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(driver);
        driver.manage().deleteCookieNamed("CONSENT");
        driver.manage().deleteCookieNamed("myCookie");
        OtherUtils.printCookies(driver);
        driver.manage().deleteAllCookies();
        OtherUtils.printCookies(driver);
    }

    @Test
    public void cookieCheckTest(){
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(driver);
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();
        OtherUtils.printCookies(driver);
    }

    @Test
    public void screenshotTest(){
        driver.get("https://google.com");
        OtherUtils.takeScreenShot(driver);
    }

    @Test
    public void alertTests() {
        driver.get(hostname + "/stubs/alert.html");
        WebElement alertButton = driver.findElement(By.id("alert-trigger"));
        WebElement confirmButton = driver.findElement(By.id("confirm-trigger"));
        WebElement promptButton = driver.findElement(By.id("prompt-trigger"));
        alertButton.click();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
        confirmButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.dismiss();
        promptButton.click();
        alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.sendKeys("aaaaaaa");
        alert.accept();
    }

    @Test
    public void hoverTest() {
        driver.get(hostname + "/stubs/hover.html");
        WebElement hoverButton = driver.findElement(By.xpath("/html/body/div/div/div[2]/div/button"));
        Actions actions = new Actions(driver);
//        actions = actions.moveToElement(hoverButton);
//        Action action = actions.build();
//        action.perform();
        actions.moveToElement(hoverButton).build().perform();
        WebElement itemMenu = driver.findElement(By.name("item 1"));
        System.out.println(itemMenu.getText());
        itemMenu.click();
    }

    @Test
    public void staleTest(){
        driver.get(hostname + "/stubs/stale.html");
        for (int i =0 ; i < 20 ; i++) {
            WebElement staleButton = driver.findElement(By.id("stale-button"));
            staleButton.click();
        }
    }

    @Test
    public void clickStealerTest() {
        driver.get(hostname + "/stubs/thieves.html");
        WebElement button = driver.findElement(By.id("the_button"));
        button.click();
        Actions actions = new Actions(driver);
        // This did not work !
        //actions.sendKeys(Keys.ESCAPE).build().perform();
        WebElement closeModal = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[3]/button"));
        // This worked for me !!
        //actions.click(closeModal).build().perform();
        WebElement closeModalX = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[1]/button"));
        // Did not work !
        //actions.click(closeModalX).build().perform();
        WebElement modalText = driver.findElement(By.xpath("//*[@id=\"myModal\"]/div/div/div[2]/p"));
        System.out.println(modalText.getText());
        // This worked for me !!
        actions.moveToElement(modalText).click(modalText).sendKeys(Keys.ESCAPE).build().perform();
        //actions.click(modalText).sendKeys(Keys.ESCAPE).build().perform();

        button.click();

    }

    @Test
    public void checkBoxTest() {
        driver.get(hostname + "/stubs/thieves.html");
        WebElement checkbox = driver.findElement(By.id("the_checkbox"));
        WebElement checkboxText = driver.findElement(By.xpath("//*[@id=\"div2\"]/label/span"));
        // Checkboxes sometimes need actions in order to be clicked
        //checkbox.click();
        Actions actions = new Actions(driver);
        actions.click(checkbox).build().perform();
        System.out.println(checkboxText.getText());
    }




}
